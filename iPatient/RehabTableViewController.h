//
//  RehabTableViewController.h
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-05-18.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RehabTableViewController : UIViewController

/*
@property (nonatomic, strong) NSArray *Back;
@property (nonatomic, strong) NSArray *Hip;
@property (nonatomic, strong) NSArray *AF;
@property (nonatomic, strong) NSArray *Core;
@property (nonatomic, strong) NSArray *Wrist;
@property (nonatomic, strong) NSArray *Shoulder;
@property (nonatomic, strong) NSArray *Elbow;
*/

@property (nonatomic, strong) NSDictionary *Stretch;
@property (nonatomic, strong) NSArray *Sections;



@end
