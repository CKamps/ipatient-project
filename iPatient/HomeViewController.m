//
//  HomeViewController.m
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-06-16.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    daysToAdd = 0;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMdd";
    today = [NSDate date];
    
    dateFormatter.dateFormat=@"MMMM";
    NSString *monthString = [[dateFormatter stringFromDate:today] capitalizedString];
    
    dateFormatter.dateFormat=@"EEEE";
    NSString *dayString = [[dateFormatter stringFromDate:today] capitalizedString];
    
    dateFormatter.dateFormat = @"dd";
    NSString *daynumber = [[dateFormatter stringFromDate:today] capitalizedString];
    
    
    Date1.text = [NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,daynumber];
}

-(IBAction)RightDate:(id)sender{
    
    daysToAdd++;
    
    NSDate *newdate = [NSDate dateWithTimeInterval:(24*60*60*daysToAdd) sinceDate:[NSDate date]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMdd";
    today = [NSDate date];
    
    dateFormatter.dateFormat=@"MMMM";
    NSString *monthString = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    dateFormatter.dateFormat=@"EEEE";
    NSString *dayString = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    dateFormatter.dateFormat = @"dd";
    NSString *daynumber = [[dateFormatter stringFromDate:newdate] capitalizedString];
    
    
    
    Date1.text = [NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,daynumber];

}
//
-(IBAction)LeftDate:(id)sender{
daysToAdd--;

NSDate *newdate = [NSDate dateWithTimeInterval:(24*60*60*daysToAdd) sinceDate:[NSDate date]];

NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
dateFormatter.dateFormat = @"yyyyMMdd";
today = [NSDate date];

dateFormatter.dateFormat=@"MMMM";
NSString *monthString = [[dateFormatter stringFromDate:newdate] capitalizedString];

dateFormatter.dateFormat=@"EEEE";
NSString *dayString = [[dateFormatter stringFromDate:newdate] capitalizedString];

dateFormatter.dateFormat = @"dd";
NSString *daynumber = [[dateFormatter stringFromDate:newdate] capitalizedString];

Date1.text = [NSString stringWithFormat:@"%@, %@ %@",dayString,monthString,daynumber];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
