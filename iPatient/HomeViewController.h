//
//  HomeViewController.h
//  iPatient
//
//  Created by Jeremy Kulchyk on 2016-06-16.
//  Copyright © 2016 Kulchapps. All rights reserved.
//

#import <UIKit/UIKit.h>


NSDate *today;
int daysToAdd;
NSDate *newdate;


@interface HomeViewController : UIViewController

{
    IBOutlet UILabel *Date1;
    IBOutlet UIButton *RightDate;
    IBOutlet UIButton *LeftDate;
    
    
}

- (IBAction)RightDate:(id)sender;
- (IBAction)LeftDate:(id)sender;


@end
